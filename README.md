
Vue Simple Comments
============

Version: 1.0.0

Introduction
------------

This is a simple comments plugin based on Vue 2.0 with rating feature and clear, responsive design.


Installation
------------

#### Add repository to `package.json`

```json
"repository": {
    "type": "git",
    "url": "https://bitbucket.org/kultprosvet/vue-simple-comments-public.git"
}
```

#### Install package 
```
npm install vue-simple-comments --save
```

#### Import **PostComments** component from **./vue-simple-comments**
```
import PostComments from './vue-simple-comments';
```

Usage
------------

#### Use **<post-comments>** tag with required props inside your Vue application
```
<post-comments :comments-list="comments" 
               :current-user="user" 
               :edit-time-limit="60">
</post-comments>
```

##### Prop **:comments-list** example:
```javascript
[
    {
        id: 1,
        approved: true,
        author: {
            id: 1,
            avatar: '',
            initials: 'JS',
            name: 'John Smith',
        },
        parent: 0,
        childrens: [
            {
                id: 2,
                approved: true,
                author: {
                    id: 2,
                    avatar: '',
                    initials: 'AW',
                    name: 'Ann White',
                },
                parent: 1,
                childrens: [],
                text: 'Thanks!',
                date: '2018-06-27 08:23:32',
                rating: 1,
                user_rating: 1
            }
        ],
        text: 'Very good article',
        date: '2018-06-27 08:05:50',
        rating: 3,
        user_rating: 0
    },
    {
        id: 3,
        approved: false,
        author: {
            id: 3,
            avatar: '',
            initials: 'AP',
            name: 'Alan Parker',
        },
        parent: 0,
        childrens: [],
        text: 'Awesome!',
        date: '2018-06-27 11:25:41',
        rating: 0,
        user_rating: 0
    }
]
```

##### Prop **:current-user** example:
```javascript
{
    id: 3,
    avatar: '',
    initials: 'AP',
    name: 'Alan Parker',
}
```

##### Prop **:dit-time-limit** set the time (in minutes) during which user can edit his comment (if no child comments have been added to this comment):

#### Listen to the **rating-change** event in the $root to get new comment rating
```javascript
this.$on('rating-change', (data) => {
    console.log(data.comment);
    console.log(data.user);
});
```

#### Listen to the **comment-edited** event in the $root to get edited comment
```javascript
this.$on('comment-edited', (comment) => {
    console.log(comment);
});
```

#### Listen to the **comment-created** event in the $root to get new comment
```javascript
this.$on('comment-created', (comment) => {
    console.log(comment);
});
```