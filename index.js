// === Import Sass ===
import './scss/main.scss';

// === Import JS Plugins ===
import Vue from 'vue';
import VueTextareaAutosize from 'vue-textarea-autosize';
import { focus } from 'vue-focus';
window.moment = require('moment');
require('moment-timezone');
require('jquery-slimscroll');

// === Config Vue ===
Vue.use(VueTextareaAutosize);

// === Import components ===
import VueSimpleComments from './components/PostComments.vue';

export default VueSimpleComments;